.. meta::
   :description: Orange Textable documentation, cookbook, display table
   :keywords: Orange, Textable, documentation, cookbook, display, table

Display table
=============

Goal
----

After some table has been created by an Orange Textable widget (for example by
counting text unit types, see :doc:`count unit frequency <count_unit_frequency>` 
or :doc:`count unit frequency in different texts or parts of texts
<count_unit_frequency_in_different_texts_parts_of_texts>`), 
display the table. 


Ingredients
-----------

  ==============  ================  =======
   **Widget**      :ref:`Convert`    **Data Table**
   **Icon**        |convert_icon|    |datatable_icon|
   **Quantity**    1                 1
  ==============  ================  =======

.. |convert_icon| image:: figures/Convert_36.png
.. |datatable_icon| image:: figures/DataTable.png


Procedure
---------

.. _display_table_fig1:

.. figure:: figures/display_table_convert_interface.png
   :align: center
   :alt: Convert to table format with an instance of Convert and Data Table

   Figure 1: Convert to table format with an instance of :ref:`Convert` and
   **Data Table**
   

.. _display_table_fig2:

.. figure:: figures/display_table_data_table_interface.png
   :align: center
   :alt: Convert to table format with an instance of Convert and Data Table

   Figure 2: **Data Table**
   
   
 
1. Create an instance of :ref:`Convert` and **Data Table** on the canvas.
2. Drag and drop from the output connection (righthand side) of the widget
   instance that has been used to import the processed units (e.g. :ref:`Count`).
3. Open the :ref:`Convert` instance's interface by double-clicking on its
   icon on the canvas.
4. Drag and drop from the output connection (righthand side) of the 
   :ref:`Convert` widget instance to the **Data Table** widget instance.
5. Open the **Data Table** instance's interface by double-clicking on its
   icon on the canvas to display your table.
   
Comment
-------
   
* If it is a frequency table, as in this example, you may want to change the
  orientation of the table to make it easier to read. To that effect, tick the 
  **Advanced settings** checkbox to access more options; then in the 
  **Transform** section, tick the **transpose** checkbox.
  
  
See also
--------

* :ref:`Reference: Convert widget <Convert>`
* :ref:`Reference: Count widget <Count>`
* :doc:`Cookbook: Count unit frequency <count_unit_frequency>` 
* :doc:`Cookbook: Count unit frequency in different texts or parts of texts
  <count_unit_frequency_in_different_texts_parts_of_texts>`

