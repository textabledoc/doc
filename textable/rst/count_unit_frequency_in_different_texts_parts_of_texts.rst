.. meta::
   :description: Orange Textable documentation, cookbook, count unit frequency 
                 in different texts or parts of texts
   :keywords: Orange, Textable, documentation, cookbook, count, frequency, 
              different, texts, parts

Count unit frequency in different texts or parts of texts
=========================================================

Goal
----

Given that two segmentations are in a hierarchical relation (e.g. a set of texts 
segmented in words or a single text segmented in both words and letters, see 
:doc:`segment text <segment_text>`), count the frequency of the smaller unit
types in the larger ones (e.g. words in texts, or letters in words).

Ingredients
-----------

  ==============  =======
   **Widget**      :ref:`Count`
   **Icon**        |count_icon|
   **Quantity**    1
  ==============  =======

.. |count_icon| image:: figures/Count_36.png


Procedure
---------

.. _count_unit_frequency_in_different_texts_parts_of_texts_fig1:

.. figure:: figures/count_frequency_in_different_texts_parts_of_texts.png
   :align: center
   :alt: Count unit frequency in differents contexts with an instance of Count

   Figure 1: Count unit frequency in different contexts with an instance of 
   :ref:`Count`.
   
 
1. Create an instance of :ref:`Count` on the canvas.
2. Drag and drop from the output connection (righthand side) of the widget
   instances that emit the segments that will be counted (e.g. :ref:`Segment`)
   to the :ref:`Count` widget instance's input connection (lefthand side).
3. Open the :ref:`Count` instance's interface by double-clicking on its
   icon on the canvas.
4. In the **Units** section, the hierarchically *lower* segments should be 
   selected in the **Segmentation** field (here: *letters*). In the **Context** 
   section, choose the mode **Containing segmentation** and the hierarchically
   *higher* units in the **Segmentation** field below (here *words*)
5. In the **Info** section appears the number of units in your segmentation 
   (here: total count is 14).
   
Comment
-------

* We assume that the segmentations here are in words and in letters (and that 
  they are labelled accordingly.
* A table showing the results is available at the output connection of the 
  widget, see :doc:`Cookbook: Display table <display_table>`.
 
  
See also
--------

* :ref:`Reference: Count widget <Count>`
* :doc:`Cookbook: Segment text <segment_text>`
* :doc:`Cookbook: Display table <display_table>`