.. meta::
   :description: Orange Textable documentation, examine evolution of unit 
                 frequency along the text
   :keywords: Orange, Textable, documentation, cookbook, evolution, unit, 
              frequency, sliding window
            

Examine the evolution of unit frequency along the text
======================================================

Goal
----

After some text has been segmented (:doc:`segment text <segment_text>`), examine
how the frequency of units evolves from the beginning to the end of the text.


Ingredients
-----------

  ==============  =======
   **Widget**      :ref:`Count`
   **Icon**        |count_icon|
   **Quantity**    1
  ==============  =======

.. |count_icon| image:: figures/Count_36.png


Procedure
---------

.. _examine_evolution_unit_frequency_along_text_fig1:

.. figure:: figures/count_unit_frequency_gradually.png
   :align: center
   :alt: Examine the evolution of unit frequency with an instance of Count

   Figure 1: Examine the evolution of unit frequency with an instance of 
   :ref:`Count`
   
 
1. Create an instance of :ref:`count` on the canvas.
2. Drag and drop from the output connection (righthand side) of the widget
   instance that has been used to segment the text 
   (e.g. :ref:`Segment`) to the :ref:`Count` widget instance's input connection 
   (lefthand side).
3. Open the :ref:`Count` instance's interface by double-clicking on its
   icon on the canvas.
4. In the **Units** section, choose the segmentation whose units will be 
   counted.
5. In the **Context** section, choose **Sliding window** in the **Mode** 
   drop-down menu.
6. Click on **Compute** to allow the widget to process the data.
7. A table showing the results is available at the output connection of the 
   widget, see :doc:`Cookbook: Display table <display_table>`.
  
   
  
See also
--------

* :ref:`Reference: Count widget <Count>`
* :doc:`Cookbook: Segment text <segment_text>`
* :doc:`Cookbook: Display table <display_table>`

