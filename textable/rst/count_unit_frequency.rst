.. meta::
   :description: Orange Textable documentation, cookbook, count unit frequency
   :keywords: Orange, Textable, documentation, cookbook, count, unit, frequency
   
   
Count unit frequency
====================

Goal
----

After some text has been segmented (:doc:`segment text <segment_text>`), count 
the types of units that appear in the text.

Ingredients
-----------

  ==============  =======
   **Widget**      :ref:`Count`
   **Icon**        |count_icon|
   **Quantity**    1
  ==============  =======

.. |count_icon| image:: figures/Count_36.png


Procedure
---------

.. _count_unit_frequency_fig1:

.. figure:: figures/count_unit_fequency_globally.png
   :align: center
   :alt: Count unit frequency globally with an instance of Count

   Figure 1: Count unit frequency globally with an instance of :ref:`Count`.
   
 
1. Create an instance of :ref:`Count` on the canvas.
2. Drag and drop from the output connection (righthand side) of the widget
   instance that emits the segments that will be counted (e.g. :ref:`Segment`)
   to the :ref:`Count` widget instance's input connection (lefthand side).
3. Open the :ref:`Count` instance's interface by double-clicking on its
   icon on the canvas.
4. In the **Units** section, your segmented text should be selected in the
   **Segmentation** field (here: *letters*).
5. A table showing the results is available at the output connection of the 
   widget, see :doc:`Cookbook: Display table <display_table>`.

Comment
-------
* In the **Info** section appears the number of units in your segmentation 
  (here: total count is 14).
 
  
See also
--------

* :ref:`Reference: Count widget <Count>`
* :ref:`Reference: Segment widget <Segment>`
* :doc:`Cookbook: Display Table <display_table>`
