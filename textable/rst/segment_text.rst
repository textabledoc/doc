.. meta::
   :description: Orange Textable documentation, cookbook, segment text
   :keywords: Orange, Textable, documentation, cookbook, segment, text

Segment text
============

Goal
----

After some text has been imported in Orange Textable (either from
:doc:`keyboard <import_text_keyboard>`, :doc:`file <import_text_file>`, or
:doc:`internet location <import_text_internet_location>`), segment text in 
lines, words, characters or letters.

Ingredients
-----------

  ==============  =======
   **Widget**      :ref:`Segment`
   **Icon**        |segment_icon|
   **Quantity**    1
  ==============  =======

.. |segment_icon| image:: figures/Segment_36.png


Procedure
---------

.. _segment_text_fig1:

.. figure:: figures/segment_text.png
   :align: center
   :alt: Segment text in different units with an instance of Segment

   Figure 1: Segment text in different units with an instance of 
   :ref:`Segment`.
   
 
1. Create an instance of :ref:`Segment` on the canvas.
2. Drag and drop from the output connection (righthand side) of the widget
   instance that has been used to import the text (e.g.
   :ref:`Text Field`, :ref:`Text Files`, or :ref:`URLs`) to the :ref:`Segment`
   instance's input connection (lefthand side).
3. Open the :ref:`Segment` instance's interface by double-clicking on its
   icon on the canvas.
4. In the **Regex** section, insert the regular expression describing the units 
   that will be segmented (for example to segment a text in lines use ``.+``, 
   in words ``\w+``, in letters ``\w`` or in characters ``.`` and so on) then 
   click on the validation button on the right (to display or export your 
   segmented text, see :doc:`Display text content <display_text_content>` or 
   :doc:`Export text content (and/or change text encoding) <export_text_content_change_encoding>` 
   in Cookbook).

Comment
-------
* In the **Regex** field you can use all the regular expression syntax Python 
  format *cf.* `Python documentation 
  <http://docs.python.org/2/library/codecs.html#standard-encodings>`_
* You can name the segmented data for future use in the **Options** section, 
  in the **Output segmentation label** field.
 
  
See also
--------

* :ref:`Reference: Segment widget <Segment>`
* :doc:`Cookbook: Import text from keyboard <import_text_keyboard>`
* :doc:`Cookbook: Import text from file <import_text_file>`
* :doc:`Cookbook: Import text from internet location
  <import_text_internet_location>`
* :doc:`Cookbook: Display text content <display_text_content>`
* :doc:`Cookbook: Export text content (and/or change text encoding) 
  <export_text_content_change_encoding>`

