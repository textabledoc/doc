﻿Credits
=======

Textable was designed and implemented by `LangTech Sàrl <http://langtech.ch>`_
on behalf of the `department of language and information sciences 
(SLI) <http://www.unil.ch/sli>`_ at the `University of Lausanne (Unil) 
<http://www.unil.ch>`_.

The largest part of funding was initially provided by the Unil's Teaching innovation
fund (`Fonds d'innovation pédagogique - FIP <http://www.unil.ch/fip>`_),
and led to the release of Textable v1.0 in summer 2012.

Textable's development has continued between 2012 and 2013, still carried on by `LangTech 
Sàrl <http://langtech.ch>`_, while the program was being gradually integrated to courses taught 
at Unil's department of `SLI <http://www.unil.ch/sli>`_ (where most of the tutorials that
would later become the :doc:`Getting started <getting_started>` section of this 
documentation have been created).
 
In automn 2013, Textable became a registered Orange Canvas add-on and was renamed to Orange 
Textable (v1.3). This promotion has made it possible to reach a much larger pool of users,
as witnessed by a steadily increasing number of downloads.

In early 2014, Unil's `FIP <http://www.unil.ch/fip>`_ has renewed its support to Orange
Textable by granting a maintenance funding. This has made it possible for `LangTech Sàrl 
<http://langtech.ch>`_ to collaborate with the creators of Orange Canvas, `University
of Ljubljana's Biolab <http://www.fri.uni-lj.si/en/laboratories/biolab/>`_ for producing 
version v1.4 of Orange Textable.

In the meantime, `Unil's Faculty of Arts <http://www.unil.ch/lettres>`_ has granted 
additional funding for translating Orange Textable's User guide from French to English,
a work which is currently under completion and based on which this online documentation
is regularly enriched.

Besides `LangTech Sàrl <http://langtech.ch>`_ and `Aris Xanthos 
<http://www.unil.ch/Jahia/site/sli/cache/bypass/pid/92240?appid=1850689_63&appparams=http%3A%2F%2Fwww.unil.ch%2Fxmlraptor%2FViews.do%3Furl_params%3D-v_faculte%3D30-v_unite%3D278-v_personne%3D11252-mode%3Dfiche&resetAppSession=true#field_1850689>`_
who have been involved at about every step of Orange Textable's conception, implementation,
documentation, and so on, a special mention should be made to Benjamin Gay (specifications, 
conception and implementation), people at `Biolab <http://www.fri.uni-lj.si/en/laboratories/biolab/>`_
(in particular Blaž Zupan and Aleš Erjavec for conception and implementation work), Corinne
Morey (French to English translation of the user guide), and many students (and a few scholars)
at `Unil <http://www.unil.ch>`_ for their indispensable feedback as users of Orange Textable.
