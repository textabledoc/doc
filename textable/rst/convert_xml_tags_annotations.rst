.. meta::
   :description: Orange Textable documentation, convert XML tags into Orange 
                 Textable annotations

   :keywords: Orange, Textable, documentation, cookbook, convert, XML, tags, 
              annotations
   

Convert XML tags into Orange Textable annotations
=================================================

Goal
----

After having imported data tagged in XML (either from
:doc:`keyboard <import_text_keyboard>`, :doc:`file <import_text_file>`, or
:doc:`internet location <import_text_internet_location>`), convert tags into 
Orange Textable annotations.  


Ingredients
-----------

  ==============  =================== 
   **Widget**      :ref:`Extract XML` 
   **Icon**        |extract_xml_icon|  
   **Quantity**    1               
  ==============  ===================

.. |extract_xml_icon| image:: figures/ExtractXML_36.png

Procedure
---------

.. _convert_XML_tags_into_Orange_Textable_annotations_fig1:

.. figure:: figures/convert_xml_tags_widget_interfaces.png
   :align: center
   :alt: Convert XML tags into Orange Textable annotations with an instance of 
         Text field and Extract XML

   Figure 1: onvert XML tags into Orange Textable annotations with an instance 
   of :ref:`Text field` and :ref:`Extract XML`
   
   
 
1. Create an instance of :ref:`Extract XML` on the canvas.
2. Drag and drop from the output connection (righthand side) of the importation 
   widget instance (either from :doc:`keyboard <import_text_keyboard>`, 
   :doc:`file <import_text_file>`, or
   :doc:`internet location <import_text_internet_location>`) to the 
   :ref:`Extract XML` widget instance's input connection (lefthand side).
3. Open the :ref:`Extract XML` instance's interface by double-clicking on its
   icon on the canvas.
4. In the **XML Extraction** section, insert the desired **XML element** 
   (here ``w``).
5. In the **Options** section, choose the **Output segmentation label** of your 
   annotated text (to display or export the extracted data, see 
   :doc:`Display <display_text_content>` or 
   :doc:`Export text content (and/or change text encoding) 
   <export_text_content_change_encoding>` in Cookbook).

 
Comment
-------
* Note that it is only possible to extract instances of a single XML element at 
  a time (here ``w``).
* However, it is possible to chain several :ref:`Extract XML` instances in order
  to successively extract instances of different XML elements. For example, a 
  first instance to extract ``div`` type elements, a second to extract ``w`` 
  type elements, and so on. In this case, it is important to make sure that the
  **Remove markup** option is *not* selected.
  
  
See also
--------

* :ref:`Reference: Extract XML widget <Extract XML>`
* :doc:`Getting started: Keyboard input and segmentation display
  <keyboard_input_segmentation_display>`
* :doc:`Cookbook: Import text from keyboard <import_text_keyboard>`
* :doc:`Cookbook: Import text from file <import_text_file>`
* :doc:`Cookbook: Import text from internet location
  <import_text_internet_location>`
* :doc:`Cookbook: Display text content <display_text_content>`
* :doc:`Cookbook: Export text content (and/or change text encoding) 
  <export_text_content_change_encoding>`

