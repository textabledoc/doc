.. meta::
   :description: Orange Textable documentation, cookbook, export text content
   :keywords: Orange, Textable, documentation, cookbook, export, text,
              content

Export text content (and/or change text encoding)
=================================================

Goal
----

After some text has been imported in Orange Textable (either from
:doc:`keyboard <import_text_keyboard>`, :doc:`file <import_text_file>`, or
:doc:`internet location <import_text_internet_location>`), export its
content.

Ingredients
-----------

  ==============  =======
   **Widget**      :ref:`Display`
   **Icon**        |display_icon|
   **Quantity**    1
  ==============  =======

.. |display_icon| image:: figures/Display_36.png

Procedure
---------

.. _export_text_content_fig1:

.. figure:: figures/export_text_content.png
   :align: center
   :alt: Export text with an instance of Display

   Figure 1: Export text with an instance of :ref:`Display`.

 
1. Create an instance of :ref:`Display` on the canvas.
2. Drag and drop from the output connection (righthand side) of the widget
   instance that has been used to import the text (e.g.
   :ref:`Text Field`, :ref:`Text Files`, or :ref:`URLs`) to the :ref:`Display`
   instance's input connection (lefthand side).
3. Open the :ref:`Display` instance's interface by double-clicking on its
   icon on the canvas to view the imported text.
4. Tick the **Advanced settings** checkbox.
5. In the **Formatting** section, tick the **Apply custom formatting** checkbox. 
6. In the **Export** section, you can choose the encoding for the text that will 
   be exported using the **File encoding** drop-down menu.
7. Click on **Export to file** button to open the file selection dialogue.
8. Select the location you want to export your file to and close the file 
   selection dialogue by clicking on **Ok**.

Comment
-------
* If you rather want to *copy* the text content in order to later paste it in 
  another program, click on **Copy to clipboard**; note that in this case, 
  the encoding is by default utf8 and cannot be changed.
* If the input data contains *several* texts (segments) you can specify a string
  that will be inserted between each successive text in **Segment delimiter**; 
  note that the default segment delimiter *\\n* represents a carriage return. 
  
See also
--------

* :ref:`Reference: Display widget <Display>`
* :doc:`Cookbook: Import text from keyboard <import_text_keyboard>`
* :doc:`Cookbook: Import text from file <import_text_file>`
* :doc:`Cookbook: Import text from internet location
  <import_text_internet_location>`

