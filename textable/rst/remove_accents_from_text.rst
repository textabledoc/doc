.. meta::
   :description: Orange Textable documentation, cookbook, remove accents from
                 text
   :keywords: Orange, Textable, documentation, cookbook, remove, accents, text

Remove accents from text
========================

Goal
----

After some text has been imported in Orange Textable (either from
:doc:`keyboard <import_text_keyboard>`, :doc:`file <import_text_file>`, or
:doc:`internet location <import_text_internet_location>`), remove all accents 
from the text.

Ingredients
-----------

  ==============  =======
   **Widget**      :ref:`Preprocess`
   **Icon**        |preprocess_icon|
   **Quantity**    1
  ==============  =======

.. |preprocess_icon| image:: figures/Preprocess_36.png

Procedure
---------

.. _remove_accents_from_text_fig1:

.. figure:: figures/remove_accents_from_text.png
   :align: center
   :alt: Remove accents from the text with an instance of Preprocess

   Figure 1: Remove accents from the text with an instance of 
   :ref:`Preprocess`.

 
1. Create an instance of :ref:`Preprocess` on the canvas.
2. Drag and drop from the output connection (righthand side) of the widget
   instance that has been used to import the text (e.g.
   :ref:`Text Field`, :ref:`Text Files`, or :ref:`URLs`) to the :ref:
   `Preprocess` instance's input connection (lefthand side).
3. Open the :ref:`Preprocess` instance's interface by double-clicking on its
   icon on the canvas.
4. In the **Processing** section, tick the **Remove accents** checkbox (to 
   display or export your modified text, see :doc:`Display text content
   <display_text_content>` or :doc:`Export text content (and/or change text 
   encoding) <export_text_content_change_encoding>` in Cookbook).

  
See also
--------

* :ref:`Reference: Preprocess widget <Preprocess>`
* :doc:`Cookbook: Import text from keyboard <import_text_keyboard>`
* :doc:`Cookbook: Import text from file <import_text_file>`
* :doc:`Cookbook: Import text from internet location
  <import_text_internet_location>`
* :doc:`Cookbook: Display text content <display_text_content>`
* :doc:`Cookbook: Export text content (and/or change text encoding) 
  <export_text_content_change_encoding>`

