.. meta::
   :description: Orange Textable documentation, cookbook, replace all 
                 occurrences of a string or pattern
   :keywords: Orange, Textable, documentation, cookbook, remove, occurrences,
              string, pattern

Replace all occurrences of a string/pattern
===========================================

Goal
----

After some text has been imported in Orange Textable (either from
:doc:`keyboard <import_text_keyboard>`, :doc:`file <import_text_file>`, or
:doc:`internet location <import_text_internet_location>`), replace all 
occurrences of a string or pattern with another.

Ingredients
-----------

  ==============  =======
   **Widget**      :ref:`Recode`
   **Icon**        |recode_icon|
   **Quantity**    1
  ==============  =======

.. |recode_icon| image:: figures/Recode_36.png


Procedure
---------

.. _replace_all_occurrences_of_string_pattern_fig1:

.. figure:: figures/replace_all_occurrences_of_string_pattern.png
   :align: center
   :alt: Replace all occurrences of a string/pattern with an instance of Recode

   Figure 1: Replace all occurrences of a string/pattern with an instance of 
   :ref:`recode`.
   
 
1. Create an instance of :ref:`Recode` on the canvas.
2. Drag and drop from the output connection (righthand side) of the widget
   instance that has been used to import the text (e.g.
   :ref:`Text Field`, :ref:`Text Files`, or :ref:`URLs`) to the :ref:`Recode`
   instance's input connection (lefthand side).
3. Open the :ref:`Recode` instance's interface by double-clicking on its
   icon on the canvas.
4. In the **Substitution** section, using a regular expression insert the string 
   that will be replaced in the **Regex** field. 
5. In the **Replacement string** field insert the replacement string (to 
   display or export your modified text, see :doc:`Display text content
   <display_text_content>` or :doc:`Export text content (and/or change text 
   encoding) <export_text_content_change_encoding>` in Cookbook).

Comment
-------
* In the **Regex** field you can use all the regular expression syntax Python 
  format *cf.* `Python documentation 
  <http://docs.python.org/2/library/codecs.html#standard-encodings>`_
* In our example, we choose to replace all occurrences of British *-our* with
  American *-or*; for example, from *colour* to *color*.
* Replacements will also occur within words, ie. *coloured* to *colored*
 
  
See also
--------

* :ref:`Reference: Recode widget <recode>`
* :doc:`Cookbook: Import text from keyboard <import_text_keyboard>`
* :doc:`Cookbook: Import text from file <import_text_file>`
* :doc:`Cookbook: Import text from internet location
  <import_text_internet_location>`
* :doc:`Cookbook: Display text content <display_text_content>`
* :doc:`Cookbook: Export text content (and/or change text encoding)
  <export_text_content_change_encoding>`

