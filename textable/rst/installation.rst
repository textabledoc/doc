Installation
============

Python v2.7 and Orange Canvas v2.7 (or more recent) must imperatively be
installed *before* Orange Textable. At the time of writing, most recent
versions of Orange Canvas automatically include the 2.7 version of Python.
After installation, Orange Textable appears in the form of an additional
tab in Orange Canvas. User guide, source files and other documents are then
accessible in your distribution of Orange, in folder
*/OrangeWidgets/Textable*.

The installation procedure is slightly different on Windows and MacOS X. [#]_

.. toctree::
    :maxdepth: 1

    Windows installation <windows_installation>
    MacOS X installation <macosx_installation>

.. [#] Even though compatibility with Linux is likely, it has not been
       specifically tested.


