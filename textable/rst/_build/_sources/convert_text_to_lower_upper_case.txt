.. meta::
   :description: Orange Textable documentation, cookbook, convert text to lower/
                 upper case
   :keywords: Orange, Textable, documentation, cookbook, convert, text,
              lower case, upper case

Convert text to lower or upper case
===================================

Goal
----

After some text has been imported in Orange Textable (either from
:doc:`keyboard <import_text_keyboard>`, :doc:`file <import_text_file>`, or
:doc:`internet location <import_text_internet_location>`), convert its
content to lower or upper case.

Ingredients
-----------

  ==============  =======
   **Widget**      :ref:`Preprocess`
   **Icon**        |preprocess_icon|
   **Quantity**    1
  ==============  =======

.. |preprocess_icon| image:: figures/Preprocess_36.png

Procedure
---------

.. _convert_text_lower_upper_case_fig1:

.. figure:: figures/convert_lower_upper_case.png
   :align: center
   :alt: Convert text to lower or upper case with an instance of Preprocess

   Figure 1: Convert text to lower or upper case with an instance of 
   :ref:`Preprocess`.

 
1. Create an instance of :ref:`Preprocess` on the canvas.
2. Drag and drop from the output connection (righthand side) of the widget
   instance that has been used to import the text (e.g.
   :ref:`Text Field`, :ref:`Text Files`, or :ref:`URLs`) to the :ref:`Preprocess`
   instance's input connection (lefthand side).
3. Open the :ref:`Preprocess` instance's interface by double-clicking on its
   icon on the canvas.
4. In the **Processing** section, tick the **Transform case** checkbox.
5. Choose **to lower** or **to upper** in the drop-down menu on the right
   (to display or export your modified text, see :doc:`Display text content
   <display_text_content>` or :doc:`Export text content (and/or change encoding) 
   <export_text_content_change_encoding>` in Cookbook).

  
See also
--------

* :ref:`Reference: Preprocess widget <Preprocess>`
* :doc:`Cookbook: Import text from keyboard <import_text_keyboard>`
* :doc:`Cookbook: Import text from file <import_text_file>`
* :doc:`Cookbook: Import text from internet location
  <import_text_internet_location>`
* :doc:`Cookbook: Display text content <display_text_content>`
* :doc:`Cookbook: Export text content (and/or change text encoding)
  <export_text_content_change_encoding>`

