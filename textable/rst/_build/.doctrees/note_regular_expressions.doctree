�cdocutils.nodes
document
q)�q}q(U	nametypesq}q(X   simple patternsq�X   compilation flagsq�X   a note on regular expressionsqNX   lookahead assertionsq	�X   more metacharatersq
�X   greedy vs. non-greedyq�X   introduction to regexesq�uUsubstitution_defsq}qUparse_messagesq]qUcurrent_sourceqNU
decorationqNUautofootnote_startqKUnameidsq}q(hUsimple-patternsqhUcompilation-flagsqhUa-note-on-regular-expressionsqh	Ulookahead-assertionsqh
Umore-metacharatersqhUgreedy-vs-non-greedyqhUintroduction-to-regexesquUchildrenq]qcdocutils.nodes
section
q)�q }q!(U	rawsourceq"U Uparentq#hUsourceq$Xe   C:\Users\Corinne Morey\Documents\UNIL\LINGUISTIQUE\Textable\textable\rst\note_regular_expressions.rstq%Utagnameq&Usectionq'U
attributesq(}q)(Udupnamesq*]Uclassesq+]Ubackrefsq,]Uidsq-]q.haUnamesq/]q0hauUlineq1KUdocumentq2hh]q3(cdocutils.nodes
title
q4)�q5}q6(h"X   A note on regular expressionsq7h#h h$h%h&Utitleq8h(}q9(h*]h+]h,]h-]h/]uh1Kh2hh]q:cdocutils.nodes
Text
q;X   A note on regular expressionsq<��q=}q>(h"h7h#h5ubaubcdocutils.nodes
paragraph
q?)�q@}qA(h"X�  Orange Textable widgets rely heavily on *regular expressions* (or *regexes*),
which are essentially a body of conventions for describing a set of strings
by means of a single string. These conventions are widely documented in books
and on the Internet, so we will not give here yet another introduction to this
topic. Nevertheless, a basic knowledge of regexes is required to perform any
non-trivial task with Orange Textable, and more advanced knowledge to
fully exploit the software's possibilities.h#h h$h%h&U	paragraphqBh(}qC(h*]h+]h,]h-]h/]uh1Kh2hh]qD(h;X(   Orange Textable widgets rely heavily on qE��qF}qG(h"X(   Orange Textable widgets rely heavily on h#h@ubcdocutils.nodes
emphasis
qH)�qI}qJ(h"X   *regular expressions*h(}qK(h*]h+]h,]h-]h/]uh#h@h]qLh;X   regular expressionsqM��qN}qO(h"U h#hIubah&UemphasisqPubh;X    (or qQ��qR}qS(h"X    (or h#h@ubhH)�qT}qU(h"X	   *regexes*h(}qV(h*]h+]h,]h-]h/]uh#h@h]qWh;X   regexesqX��qY}qZ(h"U h#hTubah&hPubh;X�  ),
which are essentially a body of conventions for describing a set of strings
by means of a single string. These conventions are widely documented in books
and on the Internet, so we will not give here yet another introduction to this
topic. Nevertheless, a basic knowledge of regexes is required to perform any
non-trivial task with Orange Textable, and more advanced knowledge to
fully exploit the software's possibilities.q[��q\}q](h"X�  ),
which are essentially a body of conventions for describing a set of strings
by means of a single string. These conventions are widely documented in books
and on the Internet, so we will not give here yet another introduction to this
topic. Nevertheless, a basic knowledge of regexes is required to perform any
non-trivial task with Orange Textable, and more advanced knowledge to
fully exploit the software's possibilities.h#h@ubeubh?)�q^}q_(h"XV  The syntax of regexes is partly standardized, but some variations remain.
Orange Textable uses Python regexes, for which Python documentation is the
best source of information. In particular, it features a good
`introduction to regexes <http://docs.python.org/2/howto/regex.html>`_. A
first reading might be limited to the following sections:h#h h$h%h&hBh(}q`(h*]h+]h,]h-]h/]uh1Kh2hh]qa(h;X�   The syntax of regexes is partly standardized, but some variations remain.
Orange Textable uses Python regexes, for which Python documentation is the
best source of information. In particular, it features a good
qb��qc}qd(h"X�   The syntax of regexes is partly standardized, but some variations remain.
Orange Textable uses Python regexes, for which Python documentation is the
best source of information. In particular, it features a good
h#h^ubcdocutils.nodes
reference
qe)�qf}qg(h"XF   `introduction to regexes <http://docs.python.org/2/howto/regex.html>`_h(}qh(UnameX   introduction to regexesUrefuriqiX)   http://docs.python.org/2/howto/regex.htmlqjh-]h,]h*]h+]h/]uh#h^h]qkh;X   introduction to regexesql��qm}qn(h"U h#hfubah&U	referenceqoubcdocutils.nodes
target
qp)�qq}qr(h"X,    <http://docs.python.org/2/howto/regex.html>U
referencedqsKh#h^h&Utargetqth(}qu(Urefurihjh-]qvhah,]h*]h+]h/]qwhauh]ubh;X=   . A
first reading might be limited to the following sections:qx��qy}qz(h"X=   . A
first reading might be limited to the following sections:h#h^ubeubcdocutils.nodes
block_quote
q{)�q|}q}(h"U h#h h$Nh&Ublock_quoteq~h(}q(h*]h+]h,]h-]h/]uh1Nh2hh]q�cdocutils.nodes
bullet_list
q�)�q�}q�(h"U h(}q�(Ubulletq�X   -h-]h,]h*]h+]h/]uh#h|h]q�(cdocutils.nodes
list_item
q�)�q�}q�(h"XN   `Simple Patterns <http://docs.python.org/2/howto/regex.html#simple-patterns>`_q�h(}q�(h*]h+]h,]h-]h/]uh#h�h]q�h?)�q�}q�(h"h�h#h�h$h%h&hBh(}q�(h*]h+]h,]h-]h/]uh1Kh]q�(he)�q�}q�(h"h�h(}q�(UnameX   Simple PatternshiX9   http://docs.python.org/2/howto/regex.html#simple-patternsq�h-]h,]h*]h+]h/]uh#h�h]q�h;X   Simple Patternsq���q�}q�(h"U h#h�ubah&houbhp)�q�}q�(h"X<    <http://docs.python.org/2/howto/regex.html#simple-patterns>hsKh#h�h&hth(}q�(Urefurih�h-]q�hah,]h*]h+]h/]q�hauh]ubeubah&U	list_itemq�ubh�)�q�}q�(h"XV   `More Metacharaters <http://docs.python.org/2/howto/regex.html#more-metacharacters>`_
h(}q�(h*]h+]h,]h-]h/]uh#h�h]q�h?)�q�}q�(h"XU   `More Metacharaters <http://docs.python.org/2/howto/regex.html#more-metacharacters>`_q�h#h�h$h%h&hBh(}q�(h*]h+]h,]h-]h/]uh1Kh]q�(he)�q�}q�(h"h�h(}q�(UnameX   More MetacharatershiX=   http://docs.python.org/2/howto/regex.html#more-metacharactersq�h-]h,]h*]h+]h/]uh#h�h]q�h;X   More Metacharatersq���q�}q�(h"U h#h�ubah&houbhp)�q�}q�(h"X@    <http://docs.python.org/2/howto/regex.html#more-metacharacters>hsKh#h�h&hth(}q�(Urefurih�h-]q�hah,]h*]h+]h/]q�h
auh]ubeubah&h�ubeh&Ubullet_listq�ubaubh?)�q�}q�(h"X#   Also recommended are the following:q�h#h h$h%h&hBh(}q�(h*]h+]h,]h-]h/]uh1Kh2hh]q�h;X#   Also recommended are the following:q���q�}q�(h"h�h#h�ubaubh{)�q�}q�(h"U h#h h$Nh&h~h(}q�(h*]h+]h,]h-]h/]uh1Nh2hh]q�h�)�q�}q�(h"U h(}q�(h�X   -h-]h,]h*]h+]h/]uh#h�h]q�(h�)�q�}q�(h"XR   `Compilation Flags <http://docs.python.org/2/howto/regex.html#compilation-flags>`_q�h(}q�(h*]h+]h,]h-]h/]uh#h�h]q�h?)�q�}q�(h"h�h#h�h$h%h&hBh(}q�(h*]h+]h,]h-]h/]uh1Kh]q�(he)�q�}q�(h"h�h(}q�(UnameX   Compilation FlagshiX;   http://docs.python.org/2/howto/regex.html#compilation-flagsq�h-]h,]h*]h+]h/]uh#h�h]q�h;X   Compilation Flagsqԅ�q�}q�(h"U h#h�ubah&houbhp)�q�}q�(h"X>    <http://docs.python.org/2/howto/regex.html#compilation-flags>hsKh#h�h&hth(}q�(Urefurih�h-]q�hah,]h*]h+]h/]q�hauh]ubeubah&h�ubh�)�q�}q�(h"XX   `Lookahead Assertions <http://docs.python.org/2/howto/regex.html#lookahead-assertions>`_q�h(}q�(h*]h+]h,]h-]h/]uh#h�h]q�h?)�q�}q�(h"h�h#h�h$h%h&hBh(}q�(h*]h+]h,]h-]h/]uh1Kh]q�(he)�q�}q�(h"h�h(}q�(UnameX   Lookahead AssertionshiX>   http://docs.python.org/2/howto/regex.html#lookahead-assertionsq�h-]h,]h*]h+]h/]uh#h�h]q�h;X   Lookahead Assertionsqꅁq�}q�(h"U h#h�ubah&houbhp)�q�}q�(h"XA    <http://docs.python.org/2/howto/regex.html#lookahead-assertions>hsKh#h�h&hth(}q�(Urefurih�h-]q�hah,]h*]h+]h/]q�h	auh]ubeubah&h�ubh�)�q�}q�(h"X_   `Greedy vs. Non-Greedy <http://docs.python.org/2/howto/regex.html#greedy-versus-non-greedy>`_

h(}q�(h*]h+]h,]h-]h/]uh#h�h]q�h?)�q�}q�(h"X]   `Greedy vs. Non-Greedy <http://docs.python.org/2/howto/regex.html#greedy-versus-non-greedy>`_q�h#h�h$h%h&hBh(}q�(h*]h+]h,]h-]h/]uh1Kh]q�(he)�q�}q�(h"h�h(}q�(UnameX   Greedy vs. Non-GreedyhiXB   http://docs.python.org/2/howto/regex.html#greedy-versus-non-greedyq�h-]h,]h*]h+]h/]uh#h�h]q�h;X   Greedy vs. Non-Greedyr   ��r  }r  (h"U h#h�ubah&houbhp)�r  }r  (h"XE    <http://docs.python.org/2/howto/regex.html#greedy-versus-non-greedy>hsKh#h�h&hth(}r  (Urefurih�h-]r  hah,]h*]h+]h/]r  hauh]ubeubah&h�ubeh&h�ubaubeubah"U Utransformerr  NUfootnote_refsr	  }r
  Urefnamesr  }r  Usymbol_footnotesr  ]r  Uautofootnote_refsr  ]r  Usymbol_footnote_refsr  ]r  U	citationsr  ]r  h2hUcurrent_liner  NUtransform_messagesr  ]r  Ureporterr  NUid_startr  KUautofootnotesr  ]r  Ucitation_refsr  }r  Uindirect_targetsr  ]r  Usettingsr   (cdocutils.frontend
Values
r!  or"  }r#  (Ufootnote_backlinksr$  KUrecord_dependenciesr%  NUrfc_base_urlr&  Uhttp://tools.ietf.org/html/r'  U	tracebackr(  �Upep_referencesr)  NUstrip_commentsr*  NUtoc_backlinksr+  Uentryr,  Ulanguage_coder-  Uenr.  U	datestampr/  NUreport_levelr0  KU_destinationr1  NU
halt_levelr2  KUstrip_classesr3  Nh8NUerror_encoding_error_handlerr4  Ubackslashreplacer5  Udebugr6  NUembed_stylesheetr7  �Uoutput_encoding_error_handlerr8  Ustrictr9  Usectnum_xformr:  KUdump_transformsr;  NUdocinfo_xformr<  KUwarning_streamr=  NUpep_file_url_templater>  Upep-%04dr?  Uexit_status_levelr@  KUconfigrA  NUstrict_visitorrB  NUcloak_email_addressesrC  �Utrim_footnote_reference_spacerD  �UenvrE  NUdump_pseudo_xmlrF  NUexpose_internalsrG  NUsectsubtitle_xformrH  �Usource_linkrI  NUrfc_referencesrJ  NUoutput_encodingrK  Uutf-8rL  U
source_urlrM  NUinput_encodingrN  U	utf-8-sigrO  U_disable_configrP  NU	id_prefixrQ  U U	tab_widthrR  KUerror_encodingrS  Ucp850rT  U_sourcerU  h%Ugettext_compactrV  �U	generatorrW  NUdump_internalsrX  NUsmart_quotesrY  �Upep_base_urlrZ  Uhttp://www.python.org/dev/peps/r[  Usyntax_highlightr\  Ulongr]  Uinput_encoding_error_handlerr^  j9  Uauto_id_prefixr_  Uidr`  Udoctitle_xformra  �Ustrip_elements_with_classesrb  NU_config_filesrc  ]Ufile_insertion_enabledrd  �Uraw_enabledre  KUdump_settingsrf  NubUsymbol_footnote_startrg  K Uidsrh  }ri  (hh�hh�hj  hhqhh�hh hh�uUsubstitution_namesrj  }rk  h&h2h(}rl  (h*]h-]h,]Usourceh%h+]h/]uU	footnotesrm  ]rn  Urefidsro  }rp  ub.