.. meta::
   :description: Orange Textable documentation, cookbook, import text from
                 file
   :keywords: Orange, Textable, documentation, cookbook, import, text,
              file

Import text from file
=====================

Goal
----

Import the content of one or more raw text files for further processing with
Orange Textable.

Ingredients
-----------

  ==============  =======
   **Widget**      :ref:`Text Files`
   **Icon**        |text_files_icon|
   **Quantity**    1
  ==============  =======

.. |text_files_icon| image:: figures/TextFiles_36.png

Procedure
---------

Single file
~~~~~~~~~~~

1. Create an instance of :ref:`Text Files` on the canvas.
2. Open its interface by double-clicking on the created instance.
3. Make sure the **Advanced settings** checkbox is *not* selected.
4. Click the **Browse** button to open the file selection dialog.
5. Select the file you want to import and close the file selection dialog by
   clicking **Ok**.
6. In the **Encoding** drop-down menu, select the encoding that corresponds to
   your file.
7. Click the **Send** button (or make sure the **Send automatically**
   checkbox is selected).
8. A segmentation covering the file's content is then available on the
   :ref:`Text Files` instance's output connections (to display or export it, 
   see :doc:`Display <display_text_content>` or :doc:`Export text content 
   (and/or change text encoding) <export_text_content_change_encoding>` in 
   Cookbook).
   
Multiple files
~~~~~~~~~~~~~~

1.  Create an instance of :ref:`Text Files` on the canvas.
2.  Open its interface by double-clicking on the created instance.
3.  Make sure the **Advanced settings** checkbox *is* selected.
4.  If needed, empty the list of imported files by clicking the **Clear all**
    button.
5.  Click the **Browse** button to open the file selection dialog.
6.  Select the files you want to import, making sure they all have the same
    encoding (you will be able to add files that have other encodings later),
    then close the file selection dialog by clicking **Ok**.
7.  In the **Encoding** drop-down menu, select the encoding that corresponds
    to the set of selected files.
8.  Click the **Add** button to add the set of selected files to the list of
    imported files.
9.  Repeat steps 5 to 8 for adding files in other encoding(s).
10. Click the **Send** button (or make sure the **Send automatically**
    checkbox is selected).
11. A segmentation containing a segment covering each imported file's content
    is then available on the :ref:`Text Files` instance's output connections
    (to display or export it, see :doc:`Display <display_text_content>` or 
    :doc:`Export text content (and/or change text encoding) 
    <export_text_content_change_encoding>` in Cookbook).

See also
--------

* :ref:`Reference: Text Files widget <Text Files>`
* :doc:`Cookbook: Display text content <display_text_content>`
* :doc:`Cookbook: Export text content (and/or change text encoding) 
  <export_text_content_change_encoding>`
