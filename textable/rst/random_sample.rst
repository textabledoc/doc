.. meta::
   :description: Orange Textable documentation, create a random selection or 
                 sample of segments

   :keywords: Orange, Textable, documentation, cookbook, create, random, 
              selection, sample, segments
   

Create a random selection or sample of segments
===============================================

Goal
----

After some text has been segmented (:doc:`segment text <segment_text>`), create 
a random sample of segments.


Ingredients
-----------

  ==============  ==============
   **Widget**      :ref:`Select` 
   **Icon**        |select_icon|  
   **Quantity**    1               
  ==============  ==============

.. |select_icon| image:: figures/Select_36.png

Procedure
---------

.. _create_random_selection_sample_of_segments_fig1:

.. figure:: figures/random_sample_Sample_mode.png
   :align: center
   :alt: Create a random selection or sample of segments with an instance of 
         Select

   Figure 1: Create a random selection or sample of segments with an instance of
   :ref:`Select`
   
   
 
1. Create an instance of :ref:`Select` on the canvas.
2. Drag and drop from the output connection (righthand side) of the segmentation 
   widget instance (:ref:`Segment`) to the :ref:`Select` widget instance's input 
   connection (lefthand side).
3. Open the :ref:`Select` instance's interface by double-clicking on its
   icon on the canvas.
4. Tick the **Advanced settings** checkbox.
5. In the **Select** section, choose the **Method** of selection **Sample**.
6. In the **Sample size expressed as** section, choose **Count**.
7. In the **Sample size** field, choose the number of elements that will be 
   randomly sampled.
8. In the **Output segmentation label** field, insert the label of your sample.
9. Click on **Send** in order to allow the widget ot process the Data 
   (to display or export the sampled data, see 
   :doc:`Display text content <display_text_content>` or 
   :doc:`Export text content (and/or change text encoding) 
   <export_text_content_change_encoding>` in Cookbook).

 
Comment
-------
*   In the **Sample size expressed as** section, you can also choose
    **Proportion**. This means that the size of the sample is expressed in a
    percentage of the total text rather than in the number of elements in the
    text.

.. _create_random_selection_sample_of_segments_fig2:

.. figure:: figures/random_sample_proportion.png
   :align: center
   :alt: Proportion expression

   Figure 2: Express sample size as a **Proportion** in the instance of
   :ref:`Select`.
  
  
See also
--------

* :ref:`Reference: Select widget <Select>`
* :doc:`Cookbook: Segment text <segment_text>`
* :doc:`Cookbook: Display text content <display_text_content>`
* :doc:`Cookbook: Export text content (and/or change text encoding) 
  <export_text_content_change_encoding>`

