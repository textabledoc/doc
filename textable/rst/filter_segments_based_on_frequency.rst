.. meta::
   :description: Orange Textable documentation, cookbook, filter segments based 
                 on their frequency

   :keywords: Orange, Textable, documentation, cookbook, filter, segments, based
              on, frequency

Filter segments based on their frequency
========================================

Goal
----

After some text has been segmented (:doc:`segment text <segment_text>`), 
include or exclude most rare (such as *hapax legomena*) or frequent segments 
(such as *the*, *a*, etc...).


Ingredients
-----------

  ==============  ============== 
   **Widget**      :ref:`Select` 
   **Icon**        |select_icon|  
   **Quantity**    1               
  ==============  ==============

.. |select_icon| image:: figures/Select_36.png

Procedure
---------

.. _filter_segments_based_on_frequency_fig1:

.. figure:: figures/filter_segments_based_on_frequency.png
   :align: center
   :alt: Filter segments based on their frequency with an instance of 
         Select

   Figure 1: Filter segments based on their frequency with an instance of
   :ref:`Select`
   
   
 
1. Create an instance of :ref:`Select` on the canvas.
2. Drag and drop from the output connection (righthand side) of the segmentation 
   widget instance (:ref:`Segment`) to the :ref:`Select` widget instance's input 
   connection (lefthand side).
3. Open the :ref:`Select` instance's interface by double-clicking on its
   icon on the canvas.
4. Tick the **Advanced settings** checkbox.
5. In the **Select** section, choose **Threshold** in the **Method** drop-down 
   list.
6. In the **Threshold expressed as** section, choose **Count** or 
   **Proportion**.
7. In the **Min. count** field, choose the minimum number of times an element
   has to appear in order not to be excluded. **Max. count** works in the same 
   way but delimiting the maximum number of time an element can appear before 
   being excluded because it is too frequent. 
8. In the **Output segmentation label** field, insert the label of your modified
   text.
9. Click on **Send** in order to allow the widget ot process the Data 
   (to display or export the modified data, see 
   :doc:`Display text content <display_text_content>` or 
   :doc:`Export text content (and/or change text encoding) 
   <export_text_content_change_encoding>` in Cookbook).

 
Comment
-------
*   In the **Threshold expressed as** section, you can either choose
    **Proportion** or **Count**. This means that the limit for an element's 
    frequency is either expressed in a percentage of the total text or in the 
    exact number of times it appears in the text.

.. _filter_segments_based_on_frequency_fig2:

.. figure:: figures/filter_segments_based_on_frequency_proportion.png
   :align: center
   :alt: Proportion expression

   Figure 2: Express Threshold as a **Proportion** in the instance of
   :ref:`Select`. 
  
  
See also
--------

* :ref:`Reference: Select widget <Select>`
* :doc:`Cookbook: Segment text <segment_text>`
* :doc:`Cookbook: Display text content <display_text_content>`
* :doc:`Cookbook: Export text content (and/or change text encoding) 
  <export_text_content_change_encoding>`

