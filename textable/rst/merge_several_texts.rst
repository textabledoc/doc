.. meta::
   :description: Orange Textable documentation, cookbook, merge several texts
   :keywords: Orange, Textable, documentation, cookbook, merge, several, texts

Merge several texts
===================

Goal
----
After some text has been imported in Orange Textable (either from
:doc:`keyboard <import_text_keyboard>`, :doc:`file <import_text_file>`, or
:doc:`internet location <import_text_internet_location>`), merge them together.

Ingredients
-----------

  ==============  ==================  
   **Widget**      :ref:`Merge`   
   **Icon**        |merge_icon|    
   **Quantity**    1                 
  ==============  ==================  
  
.. |merge_icon| image:: figures/Merge_36.png


Procedure
---------

.. _merge_several_texts_fig1:

.. figure:: figures/merge_several_texts.png
   :align: center
   :alt: Merge several texts with an instance of Merge


   Figure 1: Merge several texts with an instance of :ref:`Merge`
   


1. Create an instance of :ref:`Merge` on the canvas.
2. Drag and drop from the output connection (righthand side) of the widget
   instance that has been used to import the texts (e.g.
   :ref:`Text Field`, :ref:`Text Files`, or :ref:`URLs`) to the :ref:`Merge`
   instance's input connection (lefthand side).
3. Open the :ref:`Merge` widget instance's interface by double-clicking on its 
   icon on the canvas.
4. In the **Intersect** section, choose the **Exclude** mode.
5. In the **Ordering** section, all your texts appear. You can change the 
   ordering of the texts by selecting one and clicking on **Move Up** or **Move
   Down**.
6. In the **Options** section, choose the **Output segmentation label** of your 
   merged texts and tick the **Import labels with key** checkbox and insert a 
   key (to display or export the merged data, see 
   :doc:`Display <display_text_content>` or 
   :doc:`Export text content (and/or change text encoding) 
   <export_text_content_change_encoding>` in Cookbook).


  
See also
--------

* :ref:`Reference: Merge widget <Merge>`
* :doc:`Cookbook: Import text from keyboard <import_text_keyboard>`
* :doc:`Cookbook: Import text from file <import_text_file>`
* :doc:`Cookbook: Import text from internet location
  <import_text_internet_location>`
* :doc:`Cookbook: Display text content <display_text_content>`
* :doc:`Cookbook: Export text content (and/or change text encoding) 
  <export_text_content_change_encoding>`

