.. meta::
   :description: Orange Textable documentation, cookbook, display text content
   :keywords: Orange, Textable, documentation, cookbook, display, text,
              content

Display text content
====================

Goal
----

After some text has been imported in Orange Textable (either from
:doc:`keyboard <import_text_keyboard>`, :doc:`file <import_text_file>`, or
:doc:`internet location <import_text_internet_location>`), display its
content.

Ingredients
-----------

  ==============  =======
   **Widget**      :ref:`Display`
   **Icon**        |display_icon|
   **Quantity**    1
  ==============  =======

.. |display_icon| image:: figures/Display_36.png

Procedure
---------

.. _display_text_content_fig1:

.. figure:: figures/display_example.png
   :align: center
   :alt: Viewing text with an instance of Display

   Figure 1: Viewing text with an instance of :ref:`Display`.

1. Create an instance of :ref:`Display` on the canvas.
2. Drag and drop from the output connection (righthand side) of the widget
   instance that has been used to import the text to be displayed (e.g.
   :ref:`Text Field`, :ref:`Text Files`, or :ref:`URLs`) to the :ref:`Display`
   instance's input connection (lefthand side).
3. Open the :ref:`Display` instance's interface by double-clicking on its
   icon on the canvas to view the imported text.
   
See also
--------

* :doc:`Getting started: Keyboard input and segmentation display
  <keyboard_input_segmentation_display>`
* :ref:`Reference: Display widget <Display>`
* :doc:`Cookbook: Import text from keyboard <import_text_keyboard>`
* :doc:`Cookbook: Import text from file <import_text_file>`
* :doc:`Cookbook: Import text from internet location
  <import_text_internet_location>`

