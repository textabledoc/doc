.. meta::
   :description: Orange Textable documentation, table construction widgets
   :keywords: Orange, Textable, documentation, table, construction, widgets

Table construction widgets
==========================

Widgets of this category take *Segmentation* data in input and emit tabular
data in the internal format of Orange Textable. They are thus ultimately
responsible for converting text to tables, either by counting items
(:ref:`Count`), by measuring their length (:ref:`Length`), by quantifying
their diversity (:ref:`Variety`), or by exploiting categorical information
associated with them (:ref:`Category`). Finally, widget :ref:`Context` makes 
it possible to build concordances and collocation lists.

.. toctree::
    :maxdepth: 1

    Count <count>
    Length <length>
    Variety <variety>
    Category <category>
    Context <context>

