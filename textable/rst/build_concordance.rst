.. meta::
   :description: Orange Textable documentation, cookbook, build a concordance
   
   :keywords: Orange, Textable, documentation, cookbook, build, concordance
   

Build a concordance
===================

Goal
----

After some text has been imported in Orange Textable (either from
:doc:`keyboard <import_text_keyboard>`, :doc:`file <import_text_file>`, or
:doc:`internet location <import_text_internet_location>`), build a concordance 
table.

Ingredients
-----------

 ==============   ===============  ==============  =========
   **Widget**      :ref:`Segment`   :ref:`Select`   :ref:`Context`  
   **Icon**        |segment_icon|   |select_icon|   |context_icon|   
   **Quantity**    1                1               1                
 ==============   ===============  ==============  ========= 


.. |select_icon| image:: figures/Select_36.png
.. |segment_icon| image:: figures/Segment_36.png
.. |context_icon| image:: figures/Context_36.png


Procedure
---------

.. _build_concordance_fig1:

.. figure:: figures/build_concordance_interfaces.png
   :align: center
   :alt: Widgets used to build a concordance and their interfaces

   Figure 1: Widgets used build a concordance and their interfaces
   

1. Create an instance of :ref:`Segment`, :ref:`Select` and :ref:`Context` on 
   the canvas.
2. Connect the :ref:`Segment` widget instance to the :ref:`Select` and 
   :ref:`Context` widget instances by dragging and dropping from the output 
   connection (righthand side) of the widgets to the input connection (lefthand 
   side). 
3. Also connect the :ref:`Segment` widget and :ref:`Context` widgets together in
   order to form a triangle between the :ref:`Segment`, :ref:`Select` and 
   :ref:`Context` widgets.
4. Set the :ref:`Segment` instance to segment your text(s) in words (see
   :doc:`Cookbook: Segment text <segment_text>`)
5. In the :ref:`Select` interface, choose the **Include** mode and the **Regex**
   that will define your *key segment* in the concordance.  
6. In the **Output segmentation label** field, insert the name *key segment* or 
   other for your pivot segment in the concordance. Then click on **Send**.
7. In the :ref:`Context` interface, choose your *key segment* as the 
   **Segmentation** in the **Units** section. In the **Context** section, select 
   the **Neighboring segments** mode and the segmentation made by the your 
   earlier :ref:`Segment` widget instance (here *words*).
8. Tick the **Max. distance** checkbox in order to select the number of 
   segments taken into account on the left and right of your *key segment*.
9. Click on **Compute** to send data.
10. A table showing the results is available at the output connection of the 
    widget, see :doc:`Cookbook: Display table <display_table>`.
    

.. _build_concordance_fig2:

.. figure:: figures/build_concordance_data_table.png
   :align: center
   :alt: Concordance in Data Table

   Figure 2: Concordance displayed in **Data Table** of the word ``Hobbits`` in 
   J.R.R. Tolkien's "Concerning Hobbits" prologue.
   
   
See also
--------

* :ref:`Reference: Context widget <Context>`
* :ref:`Reference: Select widget <Select>`
* :ref:`Reference: Segment widget <Segment>`
* :doc:`Cookbook: Segment text <segment_text>`
* :doc:`Cookbook: Display table <display_table>`

