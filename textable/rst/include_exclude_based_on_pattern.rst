.. meta::
   :description: Orange Textable documentation, include or exclude units from a 
                 segmentation based on a pattern
   :keywords: Orange, Textable, documentation, cookbook, include, exclude, 
              units, segmentation, based on, pattern

Include/exclude units from a segmentation based on a pattern 
============================================================

Goal
----

After some text has been segmented (:doc:`segment text <segment_text>`), 
include or exclude units from the segmentation using a pattern.


Ingredients
-----------

  ==============  ================  
   **Widget**      :ref:`Select`   
   **Icon**        |select_icon|    
   **Quantity**    1                
  ==============  ================ 

.. |select_icon| image:: figures/Select_36.png


Procedure
---------

.. _include_exclude_units_based_on_pattern_fig1:

.. figure:: figures/include_exclude_units_based_on_pattern.png
   :align: center
   :alt: Include or exclude units based on a pattern with an instance of Select

   Figure 1: Include/exclude units from segmentation based on a pattern with an 
   instance of :ref:`Select`
   
 
1. Create an instance of :ref:`Segment` and :ref:`Select` on the canvas.
2. Connect both widgets by dragging and dropping from the output connection 
   (righthand side) of the :ref:`Segment` widget to the :ref:`Select` widget.
3. Open the :ref:`Select` instance's interface by double-clicking on its
   icon on the canvas.
4. In the **Select** section, choose the **Mode:** **Include** or **Exclude**.
5. In the **Regex** field, insert the pattern that selects the units that will 
   be included or excluded such as the letter ``e`` in our example
   (to display or export the selection, see 
   :doc:`Display <display_text_content>` or 
   :doc:`Export text content (and/or change text encoding) 
   <export_text_content_change_encoding>` in Cookbook).

   
  
See also
--------

* :ref:`Reference: Select widget <Select>`
* :doc:`Cookbook: Segment text <segment_text>`
* :doc:`Cookbook: Display text content <display_text_content>`
* :doc:`Cookbook: Export text content (and/or change text encoding) 
  <export_text_content_change_encoding>`

