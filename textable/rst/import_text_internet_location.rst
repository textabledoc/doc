.. meta::
   :description: Orange Textable documentation, cookbook, import text from
                 internet location
   :keywords: Orange, Textable, documentation, cookbook, import, text,
              internet location

Import text from internet location
==================================

Goal
----

Import text content located at one or more URLs for further processing with
Orange Textable.

Ingredients
-----------

  ==============  =======
   **Widget**      :ref:`URLs`
   **Icon**        |urls_icon|
   **Quantity**    1
  ==============  =======

.. |urls_icon| image:: figures/URLs_36.png

Procedure
---------

Single URL
~~~~~~~~~~

1. Create an instance of :ref:`URLs` on the canvas.
2. Open its interface by double-clicking on the created instance.
3. Make sure the **Advanced settings** checkbox is *not* selected.
4. In the **URL** field, type the URL whose content you want to import
   (including the ``http://`` prefix).
5. In the **Encoding** drop-down menu, select the encoding that corresponds to
   this URL.
6. Click the **Send** button (or make sure the **Send automatically**
   checkbox is selected).
7. A segmentation covering the URL's content is then available on the
   :ref:`URLs` instance's output connections (to display or export it, 
   see :doc:`Display <display_text_content>` or :doc:`Export text content 
   (and/or change text encoding) <export_text_content_change_encoding>` in 
   Cookbook).
   
Multiple URLs
~~~~~~~~~~~~~

1.  Create an instance of :ref:`URLs` on the canvas.
2.  Open its interface by double-clicking on the created instance.
3.  Make sure the **Advanced settings** checkbox *is* selected.
4.  If needed, empty the list of imported URLs by clicking the **Clear all**
    button.
5.  In the **URL(s)** field, enter the URLs you want to import (including the
    ``http://`` prefix), separated by the string " / " (space + slash +
    space); make sure they all have the same encoding (you will be able to add
    URLs that have other encodings later).
6.  In the **Encoding** drop-down menu, select the encoding that corresponds
    to the set of selected URLs.
7.  Click the **Add** button to add the set of selected URLs to the list of
    imported URLs.
8.  Repeat steps 5 to 7 for adding URLs in other encoding(s).
9.  Click the **Send** button (or make sure the **Send automatically**
    checkbox is selected).
10. A segmentation containing a segment covering each imported URL's content
    is then available on the :ref:`URLs` instance's output connections
    (to display or export it, see :doc:`Display 
    <display_text_content>` or :doc:`Export text content 
    (and/or change text encoding) <export_text_content_change_encoding>` in 
    Cookbook).

See also
--------

* :ref:`Reference: URLs widget <URLs>`
* :doc:`Cookbook: Display text content <display_text_content>`
* :doc:`Cookbook: Export text content (and/or change text encoding)
  <export_text_content_change_encoding>`
