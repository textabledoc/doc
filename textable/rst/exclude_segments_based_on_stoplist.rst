.. meta::
   :description: Orange Textable documentation, cookbook, select segments based 
                 on annotations
   :keywords: Orange, Textable, documentation, cookbook, select, segments,
              based on, annotations

Exclude segments based on a stoplist
====================================

Goal
----
After having segmented a text (:doc:`segment text <segment_text>`), exclude a 
part of the segments based on a stoplist.


Ingredients
-----------

  ==============  ==================  ===============  =======
   **Widget**      :ref:`Text Field`   :ref:`Segment`   :ref:`Intersect`
   **Icon**        |textfield_icon|    |segment_icon|   |intersect_icon|
   **Quantity**    2                   2                1
  ==============  ==================  ===============  =======

.. |textfield_icon| image:: figures/TextField_36.png
.. |segment_icon| image:: figures/Segment_36.png
.. |intersect_icon| image:: figures/intersect_36.png

Procedure
---------

.. _exclude_segments_based_on_stoplist_fig1:

.. figure:: figures/exclude_segments.png
   :align: center
   :alt: Exclude segments based on a stoplist with instances of Text Field,
         Segment and Intersect

   Figure 1: Exclude segments based on a stoplist with insances of 
   :ref:`Text Field`, :ref:`Segment` and :ref:`Intersect`
   


1. Create an instance of :ref:`Intersect` on the canvas and connect all 
   segmentation widget instances to it (i.e. :ref:`Segment`).
2. Open the widget instance's interface by double-clicking on its icon on the
   canvas.
3. In the **Intersect** section, choose the **Exclude** mode.
4. In the **Source segmentation** field, choose the segmentation of your 
   original text. Whereas in the **Filter segmentation** field, choose the label 
   of your stop words list (here: *stop words*).
5. In the **Options** section, choose the **Output segmentation label** of your 
   filtered text.
6. Click on **Send** in order to allow the widget to process the data 
   (to display or export the modified data, see 
   :doc:`Display text content <display_text_content>` or 
   :doc:`Export text content (and/or change text encoding) 
   <export_text_content_change_encoding>` in Cookbook).
   
   
Comment
-------

* A stop list of words in English can be found here 
  `<http://xpo6.com/list-of-english-stop-words/>`_


  
See also
--------

* :ref:`Reference: Intersect widget <Intersect>`
* :doc:`Cookbook: Segment text <segment_text>`
* :doc:`Cookbook: Display text content <display_text_content>`
* :doc:`Cookbook: Export text content (and/or change text encoding) 
  <export_text_content_change_encoding>`

