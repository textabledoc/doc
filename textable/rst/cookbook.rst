Cookbook
========

Text input
----------

.. toctree::
    :maxdepth: 1

    Import text from keyboard <import_text_keyboard>
    Import text from file <import_text_file>
    Import text from internet location <import_text_internet_location>
    Convert XML tags to Orange Textable annotations <convert_xml_tags_annotations>

Text output
-----------

.. toctree::
    :maxdepth: 1

    Display text content <display_text_content>
    Export text content (and/or change text encoding) <export_text_content_change_encoding>

Text modifications and processing
---------------------------------

.. toctree::
    :maxdepth: 1

    Segment text <segment_text>
    Convert text to lower or upper case <convert_text_to_lower_upper_case>
    Remove accents from text <remove_accents_from_text>
    Replace all occurrences of a string/pattern <replace_all_occurrences_of_string_pattern>
    Merge several texts <merge_several_texts>
    Include/exclude units from a segmentation based on a pattern <include_exclude_based_on_pattern>
    Exclude segments based on a stoplist <exclude_segments_based_on_stoplist>
    Filter segments based on their frequency <filter_segments_based_on_frequency>

Text statistics
---------------

.. toctree::
    :maxdepth: 1
   
    Count unit frequency <count_unit_frequency>
    Count unit frequency in different texts or parts of texts <count_unit_frequency_in_different_texts_parts_of_texts>
    Count occurrences of smaller units in larger segments <count_occurrences_smaller_units_larger_segments>
    Count transition frequency between adjacent units <count_transition_frequency_adjacent_units>
    Examine the evolution of unit frequency along the text <examine_evolution_unit_frequency>
    Create a random selection or sample of segments <random_sample>
    
Build tables
------------

.. toctree::
    :maxdepth: 1   
     
    Display table <display_table>
    Export table <export_table>
    Build a concordance <build_concordance>