.. meta::
   :description: Orange Textable documentation, count transition frequency 
                 between adjacent units 
   :keywords: Orange, Textable, documentation, cookbook, count, transition, 
              frequency, Markov Chain

Count transition frequency between adjacent units 
=================================================

Goal
----

After some text has been segmented (:doc:`segment text <segment_text>`), count
the frequency of transitions between adjacent units.


Ingredients
-----------

  ==============  =======
   **Widget**      :ref:`Count`
   **Icon**        |count_icon|
   **Quantity**    1
  ==============  =======

.. |count_icon| image:: figures/Count_36.png


Procedure
---------

.. _count_transition_frequency_between_adjacent_units_fig1:

.. figure:: figures/count_frequency_adjacent_contexts.png
   :align: center
   :alt: Count frequency of adjacent contexts with an instance of Count

   Figure 1: Count transition frequency with an instance of :ref:`Count`
   
 
1. Create an instance of :ref:`Count` on the canvas.
2. Drag and drop from the output connection (righthand side) of the widget
   instance that has been used to segment the text
   (e.g. :ref:`Segment`) to the :ref:`Count` widget instance's input connection 
   (lefthand side).
3. Open the :ref:`Count` instance's interface by double-clicking on its
   icon on the canvas.
4. In the **Units** section, choose the segmentation in which transitions 
   between units will be counted.
5. In the **Context** section, choose **Left-right neighborhood** in the 
   **Mode** drop-down menu.
6. In the **Left context size** choose *1*, and in the **Right context size** 
   field, choose *0*.
7. Click on **Compute** to allow the widget to process the data.
8. A table showing the results is available at the output connection of the 
   widget, see :doc:`Cookbook: Display table <display_table>`.

  
Comment
-------
* It is also possible to count the apparition of units in more complex contexts 
  than simply the previous unit, such as: the *n* previous units (**Left context 
  size**); the *n* following units (**Right context size**); or any combination 
  of both.
* The **Unit position marker** is a string that indicates the separation between
  left and right contexts sides. The default is ``_`` but you can change it by 
  inserting the marker of your choice. 

   
  
See also
--------

* :ref:`Reference: Count widget <Count>`
* :doc:`Cookbook: Segment text <segment_text>`
* :doc:`Cookbook: Display table <display_table>`

