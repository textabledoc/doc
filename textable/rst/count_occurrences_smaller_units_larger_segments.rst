.. meta::
   :description: Orange Textable documentation, count occurrences of smaller 
                 units in larger segments
   :keywords: Orange, Textable, documentation, cookbook, count, occurrences, 
              units, contexts, term-document matrix

Count occurrences of smaller units in larger segments
=====================================================

Goal
----

After some text has been segmented in at least two different ways
(:doc:`segment text <segment_text>`), count the occurrences of the smaller
units (for instance ``letters``) in the larger segments 
(for instance ``words``).


Ingredients
-----------

  ==============  =============
   **Widget**      :ref:`Count`   
   **Icon**        |count_icon|  
   **Quantity**    1          
  ==============  =============

.. |count_icon| image:: figures/Count_36.png

Procedure
---------

.. _count_occurrences_smaller_units_in_larger_segments_fig1:

.. figure:: figures/count_occurrences_other_smaller_segmentation.png
   :align: center
   :alt: Count occurrences of a smaller units in larger segments with an 
         instance of Count

   Figure 1: Count occurrences of smaller units in larger segments with an 
   instance of :ref:`Count`
   
 
1. Create an instance of :ref:`Count` on the canvas.
2. Drag and drop from the output connection (righthand side) of both widget
   instances that have been used to segment the text
   (e.g. :ref:`Segment`) to the :ref:`Count` widget instance's input connection 
   (lefthand side).
3. Open the :ref:`Count` instance's interface by double-clicking on its
   icon on the canvas.
4. In the **Units** section, choose the segmentation, i.e, the 
   smaller segmentation.
5. In the **Context** section, choose the **Containing segmentation** in the 
   **Mode** drop-down menu.
6. In the **Segmentation** field, choose the context segmentation, i.e. the 
   larger segmentation.
7. Click on **Compute** to allow the widget to process the data.
8. A table showing the results is available at the output connection of the 
   widget, see :doc:`Cookbook: Display table <display_table>`.


  
See also
--------

* :ref:`Reference: Count widget <Count>`
* :doc:`Cookbook: Segment text <segment_text>`
* :doc:`Cookbook: Display table <display_table>`

