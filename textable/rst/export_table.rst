.. meta::
   :description: Orange Textable documentation, cookbook, export table
   :keywords: Orange, Textable, documentation, cookbook, export, table

Export table
=============

Goal
----

After some table has been created by an Orange Textable widget (for example by
counting text unit types, see :doc:`count unit frequency <count_unit_frequency>` 
or :doc:`count unit frequency in different texts or parts of texts
<count_unit_frequency_in_different_texts_parts_of_texts>`), export the table. 


Ingredients
-----------

  ==============  ================  =======
   **Widget**      :ref:`Convert`    **Data Table**
   **Icon**        |convert_icon|    |datatable_icon|
   **Quantity**    1                 1
  ==============  ================  =======

.. |convert_icon| image:: figures/Convert_36.png
.. |datatable_icon| image:: figures/DataTable.png


Procedure
---------

.. _export_table_fig1:

.. figure:: figures/export_table_convert_interface.png
   :align: center
   :alt: Export table with an instance of Convert

   Figure 1: Export table with an instance of :ref:`Convert`
   
 
1. Create an instance of :ref:`Convert` and **Data Table** on the canvas.
2. Drag and drop from the output connection (righthand side) of the widget
   instance that has been used to import the processed units (e.g. :ref:`Count`).
3. Open the :ref:`Convert` instance's interface by double-clicking on its
   icon on the canvas.
4. Tick the **Advanced settings** checkbox to access more options.
5. Click on **Export to file** button to open the file selection dialogue.
6. Select the location you want to export your file to and close the file 
   selection dialogue by clicking on **Ok**.
   

Comment
-------
* The **Column delimiter** is by default \\t but this can be modified to either
  comma (,) or semi-colon (;).
* If you rather want to *copy* the text content in order to later paste it in 
  another program, click on **Copy to clipboard**; note that in this case, 
  the encoding is by default utf8 and cannot be changed.
   
  
See also
--------

* :ref:`Reference: Convert widget <Convert>`
* :ref:`Reference: Count widget <Count>`
* :doc:`Cookbook: Count unit frequency <count_unit_frequency>` 
* :doc:`Cookbook: Count unit frequency in different texts or parts of texts
  <count_unit_frequency_in_different_texts_parts_of_texts>`

