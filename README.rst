Orange Textable
===============

Orange Textable is an add-on for Orange_ data mining software package. It
enables users to build data tables on the basis of text data, by means of a
flexible and intuitive interface. Look at the following `example
<http://orange-textable.readthedocs.org/en/latest/illustration.html>`_ to see
it in typical action.

Orange Textable offers in particular the following features:

- text data import from keyboard, files, or urls
- systematic recoding
- segmentation and annotation of various text units
- extract and exploit XML-encoded annotations
- automatic, random, and arbitrary selection of unit subsets
- unit context examination using concordance and collocation tables
- frequency and complexity measures
- recoded text data and table export

.. _Orange: http://orange.biolab.si/

Documentation is found at:

http://orange-textable.readthedocs.org/

Textable was designed and implemented by `LangTech Sarl <http://langtech.ch>`_
on behalf of the department of language and information
sciences (SLI_) at the `University of Lausanne <http://www.unil.ch>`_ (see
`Credits <http://orange-textable.readthedocs.org/en/latest/credits.html>`_
and `How to cite Orange Textable
<http://orange-textable.readthedocs.org/en/latest/credits.html>`_).

.. _SLI: http://www.unil.ch/sli

